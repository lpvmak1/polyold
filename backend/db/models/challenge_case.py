from django.db import models

from .base import BaseModel
from .challenge import Challenge


class ChallengeCase(BaseModel):
    """
    Модель задачи для решения
    """
    challenge = models.ForeignKey(Challenge, on_delete=models.CASCADE)
    input = models.TextField()
    output = models.TextField()
    is_example = models.BooleanField(default=False)

    class Meta:
        default_related_name = 'challenge_cases'
        db_table = 'challenge_cases'
        verbose_name_plural = "тесткейсы"
        verbose_name = "тесткейс"
