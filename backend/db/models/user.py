from django.contrib.auth.models import AbstractUser, UserManager as BaseUserManager
from django.db import models
from django.db.models import Sum, F, Q, OuterRef

from .base import BaseModel
from django.utils.translation import gettext_lazy as _


class UserManager(BaseUserManager):
    def get_queryset(self):
        from .challenge import Challenge
        return super(UserManager, self).get_queryset()


class User(AbstractUser, BaseModel):
    """
    Модель пользователя
    """
    email = models.EmailField(_('email address'), blank=True, unique=True)
    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['username']

    objects = UserManager()

    @property
    def score(self):
        from . import Challenge
        return Challenge.objects.filter(solutions__user=self, solutions__success=True).distinct().aggregate(Sum('score_points'))['score_points__sum']

    class Meta:
        db_table = 'users'
        default_related_name = 'users'
        verbose_name_plural = "пользователи"
        verbose_name = "пользователь"
