from django.db import models


class BaseModel(models.Model):
    """
    Базовый класс для всех моделей проекта
    """
    created_on = models.DateTimeField(auto_now_add=True)
    updated_on = models.DateTimeField(auto_now=True)

    def __str__(self):
        return "{}#{}".format(self._meta.verbose_name or self.__class__.__name__, self.pk)

    class Meta:
        abstract = True
