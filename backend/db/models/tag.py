from django.db import models

from db.models import BaseModel


class Tag(BaseModel):
    """
    Модель тега для задачи
    """
    name = models.CharField(max_length=128, verbose_name="Название тега", unique=True)

    def __str__(self):
        return self.name

    class Meta:
        db_table = 'tags'
        default_related_name = 'tags'
        verbose_name_plural = "теги"
        verbose_name = "тег"
