import os

from celery import Celery

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'polyold.settings')

app = Celery('polyold')

app.config_from_object('django.conf:settings', namespace='CELERY')

app.autodiscover_tasks()