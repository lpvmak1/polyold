from rest_framework import viewsets

from api.serializers.challenge import ChallengeSerializer
from db.models import Challenge


class ChallengeViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = Challenge.objects.all().order_by('score_points')
    serializer_class = ChallengeSerializer
