from drf_spectacular.utils import extend_schema, extend_schema_field
from rest_framework.fields import SerializerMethodField
from rest_framework.relations import StringRelatedField
from rest_framework.serializers import ModelSerializer

from api.serializers.challenge_case import ChallengeCaseSerializer, ShortChallengeCaseSerializer
from db.models import Challenge


class ChallengeSerializer(ModelSerializer):
    tags = StringRelatedField(many=True)
    examples = SerializerMethodField()

    @extend_schema_field(ShortChallengeCaseSerializer(many=True))
    def get_examples(self, instance):
        return ShortChallengeCaseSerializer(instance.challenge_cases.filter(is_example=True), many=True).data

    class Meta:
        model = Challenge
        fields = '__all__'
