from rest_framework.relations import StringRelatedField
from rest_framework.serializers import ModelSerializer
from db.models import Challenge, ChallengeCase


class ChallengeCaseSerializer(ModelSerializer):
    class Meta:
        model = ChallengeCase
        fields = '__all__'


class ShortChallengeCaseSerializer(ModelSerializer):
    class Meta:
        model = ChallengeCase
        fields = ('input', 'output')
