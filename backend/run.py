import os

if __name__ == '__main__':
    os.environ.setdefault("DJANGO_SETTINGS_MODULE", "polyold.settings")
    from django.core.management import call_command
    from django.core.wsgi import get_wsgi_application

    application = get_wsgi_application()
    call_command('migrate')
    call_command('collectstatic', '--noinput')
    call_command('runserver', f'0.0.0.0:8000')
