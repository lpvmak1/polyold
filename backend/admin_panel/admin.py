from django.contrib import admin
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin
from django.db.models import Sum, Q, F
from django.utils.translation import gettext, gettext_lazy as _

from import_export.admin import ImportExportModelAdmin, ImportExportMixin
from import_export import resources

from core.admin import DefaultModelAdmin, EditReadOnlyAdmin
from db import models
from db.models import User


class ChallengeResource(resources.ModelResource):
    class Meta:
        model = models.Challenge


class ChallengeCaseAdminInline(admin.StackedInline):
    model = models.ChallengeCase


class ChallengeAdmin(ImportExportMixin, DefaultModelAdmin):
    resource_class = ChallengeResource

    inlines = (ChallengeCaseAdminInline,)


class SolutionAdmin(EditReadOnlyAdmin, DefaultModelAdmin):
    pass


class TagAdmin(DefaultModelAdmin):
    pass


class ChallengeCaseResource(resources.ModelResource):
    class Meta:
        model = models.ChallengeCase


class ChallangeCaseAdmin(ImportExportMixin, DefaultModelAdmin):
    resource_class = ChallengeCaseResource
    pass


class UserAdmin(BaseUserAdmin):
    fieldsets = (
        (None, {'fields': ('username', 'password', 'score')}),
        (_('Personal info'), {'fields': ('first_name', 'last_name', 'email')}),
        (_('Permissions'), {
            'fields': ('is_active', 'is_staff', 'is_superuser', 'groups', 'user_permissions'),
        }),
        (_('Important dates'), {'fields': ('last_login', 'date_joined')}),
    )
    readonly_fields = ('score',)

    list_display = (*BaseUserAdmin.list_display, 'score')

    def score(self, instance):
        return instance.score or 0

    score.short_description = 'Очки'

    def get_queryset(self, request):
        return User.objects.all()



admin.site.register(models.Challenge, ChallengeAdmin)
admin.site.register(models.ChallengeCase, ChallangeCaseAdmin)
admin.site.register(models.User, UserAdmin)
admin.site.register(models.Solution, SolutionAdmin)
admin.site.register(models.Tag, TagAdmin)
