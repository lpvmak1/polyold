from django.apps import AppConfig
from django.db.models.signals import post_save


class ServiceConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'service'

    def ready(self):
        from service.signals import solution_after_create
        from db.models import Solution

        post_save.connect(solution_after_create, sender=Solution, dispatch_uid="compile_and_test")
