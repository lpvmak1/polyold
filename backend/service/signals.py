from django.db.models.signals import post_save
from django.dispatch import receiver

from db.models import Solution
from service.services import CheckSolutionService

lock = set()


def solution_after_create(sender, instance, *args, **kwargs):
    if not instance.pk in lock:
        lock.add(instance.pk)
        CheckSolutionService(solution=instance).check_solution()
        lock.remove(instance.pk)
