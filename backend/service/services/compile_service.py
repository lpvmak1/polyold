import os
import uuid
from logging import getLogger

from django.core.files import File

logger = getLogger(__name__)


class CompileService:
    def __init__(self, file: File, folder):
        self.file = file
        self.folder_name = folder
        self._folder = f"./media/test_solutions/{self.folder_name}"
        self._compiled = f"{self._folder}/compiled"
        self.compile_error = None

    def compile(self):
        ret = os.system("mkdir ./media/test_solutions/")
        ret = os.system(f"mkdir {self._folder}")
        ret = os.system(f"gcc ./media/{self.file} -o {self._compiled} > {self._folder}/compile_error")
        if ret != 0:
            self.compile_error = f"Error {ret}:"
            try:
                with open(f"{self._folder}/compile_error", "r") as f:
                    self.compile_error += f.read()
            except Exception as e:
                logger.exception(e)

    def exec(self, stdin) -> str:
        uuid_file = str(uuid.uuid4())
        with open(f"{self._folder}/{uuid_file}", "w") as f:
            f.write(stdin)
        ret = os.system(f"{self._compiled} < {self._folder}/{uuid_file} > {self._folder}/output")
        result = ""
        with open(f"{self._folder}/output") as f:
            result = f.read()
        return result
