from db import models
from service.services.compile_service import CompileService


class CheckSolutionService:
    def __init__(self, solution: models.Solution):
        self.solution = solution
        self.complile_service = CompileService(file=self.solution.file, folder=solution.id)

    def check_solution(self):
        self.complile_service.compile()
        error = ''
        if self.complile_service.compile_error:
            error = self.complile_service.compile_error

        test_cases = self.solution.challenge.challenge_cases.all()
        success = 0
        all = test_cases.count()
        if not error:
            for test_case in test_cases:
                result = self.complile_service.exec(test_case.input)
                if result != test_case.output:
                    error = 'Тест не пройден!!!'
                else:
                    success += 1
        if error:
            error += f"({success}/{all})"
            self.solution.comment = error
        else:
            self.solution.success = True
        self.solution.save()
