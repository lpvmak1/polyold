# PolyOld

## Описание

Проект направлен на создание платформы для решения задач по программированию для студентов Прикладной математики и информатики СПбПУ Петра Великого


## Инструкция по развертыванию

- Установить docker и docker-compose

https://docs.docker.com/engine/install/

https://docs.docker.com/compose/install/

- Развернуть проект используя docker-compose

```bash
#Из директории с проектом

docker-compose up -d
```

- Проверить, что все ок: зайти на http://127.0.0.1:8000/admin

## Документация

- [Схема БД](./docs/db_schema.svg)
- [Swagger документация](https://polyold.lapotnikov.ru/api/schema/swagger-ui/)
