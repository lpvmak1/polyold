# Документация для Frontend

## frontend/src/pages/Tasks/ListTask

### Table.js 
#### Процедура props =>
Принимает пропс с данным о таблице (Task, Score, Level).

Создает таблицу с 3 колонками и заполняет ее значениями из пропса.
Task: названия заданий. При нажатии позволяет перейти на 'frontend/src/pages/Tasks/TaskDescription/index.jsx'.
Score: количество очков.
Level: уровени сложности. 

### index.js 
#### class Heder 
##### Метод componentDidMount()
Делает запррос к Api 'https://polyold.lapotnikov.ru/api/challenges/' и получает json файл с зданиями.

##### {
#####    name: string 
#####    score_points: int
#####    tags: []
##### }

##### Метод render()
Производит проверку получилось ли получить данные из Api.
Возращает процедуру props из 'frontend/src/pages/Tasks/ListTask/index.js'.

## frontend/src/pages/Tasks/TaskDescription
#### Процедура TaskDescription 

##### функция refreshMySolutions()
Делает запрос к Api 'https://polyold.lapotnikov.ru/api/challenges/id' получает json файл с зданиями.

##### {
#####    name: string 
#####    score_points: int
#####    tags: []
##### }

##### функция dargStartHandler(e)
Принимает состояние drag and drop
Заменяет состояние на заначение true
##### функция dargLeaveHandler(e)
Принимает состояние drag and drop
Заменяет состояние на заначение false
##### функция onDropHandler(e)
Принимает состояние drag and drop
Делает запрос к Api 'https://polyold.lapotnikov.ru/api/solutions/'b и отправляет файл с id пользователя.
Возвращает 4 таблицы:

##### Первая таблица
Создает таблицу с 3 колонками и заполняет ее значениями из пропса.
Task: названия заданий.
Score: количество очков.
Level: уровени сложности. 
##### Вторая таблица
Description: подробное описание задания
##### Третья таблица
Send file: отправляет файл.  
##### Четвертая таблица
File: номер отправленого файла.
Created On: дата и время отправки файла.
Comment: количество пройденых тестоов.
Success: успешность или неуспешность отправленого решения. 

## frontend/src/routes/components/GuestRoute/index.js
### Функция GuestRoute
Принимает состояние авторизованных пользователей.
Предоставляет возможности пользования приложения для авторизованных пользователей. 

## frontend/src/routes/components/PrivateRoute/index.js
### Функция GuestRoute
Принимает состояние неавторизованных пользователей.
Предоставляет возможности пользования приложения для неавторизованных пользователей. 

## frontend/src/routes/components/Routes/index.jsx
### Функция AppRoutes()
Возвращает пути для перехада между страницами сайта

## frontend/src/endpoints/atho.js
### перменная endpoints
Переменная которая хранит запросы к backend:
#### registration: отправляет пост запрос для регистрации.
#### login: отправляет пост запррос для авторизации.
#### getProfile: отправяелт гет запрос для полученния данных профиля.
#### updateProfile: отправляет пост запрос для обновления данных пользователя.

## frontend/src/axios/axios.js
### axiosInstance.interceptors.request.use
Метод возвращает cookie файлы для того, чтобы не проходить повторную авторизацию.

## frontend/App.js
Функция App()
Проверяет на то авторизован пользователь или нет. 
Возвращает шапку страницы с копками. 
Кнопки на странице, если пользователь авторизирован:
### Logo(изображение): при нажатии перенраправляет на страницу home.
### Tasks: при нажатии перенраправляет на страницу task.
### Name/Surname: при нажатии перенраправляет на страницу profile.
### Log out: при нажатии пользователь выходит с приложения.

Кнопки на странице, если пользователь не авторизирован:
### Logo(изображение): при нажатии перенраправляет на страницу home.
### Login: при нажатии перенраправляет на страницу login.
### Registration: при нажатии перенраправляет на страницу registration.