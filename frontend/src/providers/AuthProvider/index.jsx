import { useCallback, useEffect, useMemo, useState } from "react";
import Cookies from "js-cookie";
import { AuthContext } from "../../contexts/AuthContext";
import api from "../../services/api";

function AuthProvider(props) {
  const [isLoaded, setIsLoaded] = useState(false);
  const [user, setUser] = useState(null);
  const [access, setAccessData] = useState(null);

  const setAccess = useCallback((accessData) => {
    setAccessData(accessData);

    if (accessData) {
      Cookies.set("auth-access", accessData);
    } else {
      Cookies.remove("auth-access");
    }
  }, []);

  const logOut = useCallback(() => {
    setUser(null);
    setAccess(null);
  }, [setAccess]);

  const loadData = useCallback(async () => {
    const accessData = Cookies.get("auth-access");
    setAccessData(accessData);
    console.log('Я живу');
    try {
      if (accessData) {
        const { data } = await api.auth.getProfile();
        setUser(data);
      }
    } catch {
      setAccess(null);
    } finally {
      setIsLoaded(true);
    }
  }, [setUser, setAccess, setIsLoaded]);

  useEffect(() => {
    loadData();
  }, [loadData]);

  const contextValue = useMemo(
    () => ({
      isLoaded,
      user,
      access,
      setUser,
      setIsLoaded,
      setAccess,
      logOut,
    }),
    [isLoaded, user, access, setAccess, setUser, setIsLoaded, logOut]
  );

  return (
    <AuthContext.Provider value={contextValue}>
      {props.children}
    </AuthContext.Provider>
  );
}

export default AuthProvider;
