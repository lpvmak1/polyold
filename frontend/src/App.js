import {
  AppBar,
  Toolbar,
  Typography,
  Button,
  makeStyles,
} from "@material-ui/core";
import { Link, useNavigate } from "react-router-dom";
import "./App.css";
import Routes from "./routes/Routes";
import useAuth from "./hooks/useAuth";
import logo from './logo.png';

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  rightToolbar: {
    flexGrow: 1,
  },
  title: {
    marginRight: theme.spacing(2),
  },
}));

function App() {
  const classes = useStyles();
  const auth = useAuth();
  const navigate = useNavigate();

  const onLogOut = () => {
    auth.logOut();
    navigate("/login");
  };

  return (
    <div className={classes.root}>
      <AppBar position="static">
        <Toolbar>
          <div>
            <Button color="inherit" component={Link} to="/">
              <img src = {logo} width = {120} height = {35} alt = "Polyold"/>
            </Button>
          </div>
          {auth.isLoaded &&
            (auth.user ? (
              <>
              <div className={classes.rightToolbar}>
                <Button color="inherit" component={Link} to="/tasks">
                  Tasks
                </Button>
              </div>
                 Score: {auth.user.score} 
                <Button color="inherit" component={Link} to="/profile">
                {auth.user.first_name} {auth.user.last_name} 
                </Button>
                <Button color="inherit" onClick={onLogOut}>
                  Log out
                </Button>
              </>
            ) : (
              <>
                <Button className={classes.rightToolbar} color="inherit" component={Link} to="/login">
                  Login
                </Button>
                <Button className={classes.rightToolbar} color="inherit" component={Link} to="/registration">
                  Registration
                </Button>
              </>
            ))}
        </Toolbar>
      </AppBar>

      <Routes />
    </div>
  );
}

export default App;
