import axios from "../axios";

const endpoints = {
  registration: (data) => axios.post("/api/register/", data),
  login: (data) => axios.post("/api/token/", data),
 
  getProfile: () => axios.get("/api/current-user/"),
  
  updateProfile: (data) => axios.patch("/api/current-user/", data),
};

export default endpoints;