import axios from "axios";
import Cookies from "js-cookie";

const axiosInstance = axios.create({
  baseURL: "https://polyold.lapotnikov.ru/",
});

axiosInstance.interceptors.request.use(
  (config) => {
    const authAccess = Cookies.get("auth-access");

    if (authAccess) {
      config.headers.authorization = `Bearer ${authAccess}`;
    }

    return config;
  },
  (error) => Promise.reject(error)
);

export default axiosInstance;