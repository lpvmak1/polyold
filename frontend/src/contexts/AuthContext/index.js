import { createContext } from "react";

export const AuthContext = createContext({
  isLoaded: false,
  user: null,
  access: null,
  setUser: () => {},
  setAccess: () => {},
  setIsLoaded: () => {},
  logOut: () => {},
});
