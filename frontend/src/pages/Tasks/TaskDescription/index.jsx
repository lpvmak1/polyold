import { useEffect, useState} from "react";
import { useParams } from "react-router-dom";
import {
  Typography,
  Button,
  Table,
  TableBody, 
  TableCell, 
  TableContainer, 
  TableHead, 
  TableRow, 
  makeStyles,
  Paper
} from "@material-ui/core";
import "./border.css"
import axios from "../../../services/api/axios";
import useAuth from "../../../hooks/useAuth"


const useStyles = makeStyles((theme) => ({
  table: {
    minWidth: 1,
  },
  tableContainer: {
    borderRadius: 15,
    margin: '10px auto',
    maxWidth: 700,
  },
  tableHeaderCell: {
    fontWeight: 'bold',
    backgroundColor: theme.palette.primary.dark,
    color: theme.palette.getContrastText(theme.palette.primary.dark),
  }, 
  status: {
    fontWeight: 'bold',
    fontSize: '0.75rem',
    color: 'white',
    backgroundColor: 'grey',
    borderRadius: 8,
    padding: '3px 10px',
    display: 'inline-block'
  }
}));



const TaskDescription = () => {
  const {id} = useParams();
  const classes = useStyles();
  const [post, SetPost] = useState(null);
  const auth = useAuth()
  const [mySolutions, setMySolutions] = useState([]);
  useEffect(() => {
    fetch(`https://polyold.lapotnikov.ru/api/challenges/${id}`)
    .then(res => res.json())
    .then(data => SetPost(data))

  }, [id]);
  function refreshMySolutions(){
    if(auth?.user && post?.id)
    axios.get(`/api/solutions/?user=${auth.user?.id}&challenge=${post?.id}&ordering=-created_on`).then(data=>{
      console.log(data)
      setMySolutions(data.data)
    });
  }
  useEffect(() => {
    refreshMySolutions()
  }, [post, auth])
  const [darg, setDrag] = useState(false)

  function dargStartHandler(e){
    e.preventDefault()
    setDrag(true)
  }
  function dargLeaveHandler(e){
    e.preventDefault()
    setDrag(false)
  }
  function onDropHandler(e){
    e.preventDefault()
    let files = [...e.dataTransfer.files]
   const formData = new FormData();
    formData.append('file', files[0])
    formData.append('challenge', id)
    console.log({id})
    axios.post('https://polyold.lapotnikov.ru/api/solutions/', formData ).then(()=>{
      refreshMySolutions()
    })

    setDrag(false)
  }
  console.log(mySolutions)
  return(
    <div>
      {post && (
      <>
      <TableContainer component={Paper} className={classes.tableContainer}>
        <Table className={classes.table}>
          <TableHead>
            <TableRow>
              <TableCell className={classes.tableHeaderCell}>Task</TableCell>
              <TableCell className={classes.tableHeaderCell}>Score</TableCell>
              <TableCell className={classes.tableHeaderCell}>Level</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            <TableCell>
                <Typography variant="body1">
                      {post.name}
                </Typography>
            </TableCell>
            <TableCell>{post.score_points}</TableCell>
              <TableCell>
                <Typography
                  className={classes.status} 
                  style={{
                        backgroundColor: 
                        ((post.tags == 'Easy' && 'green') ||
                        (post.tags == 'Normal' && 'orange') ||
                        (post.tags == 'Hard' && 'red'))
                      }}>
                      {post.tags}
                </Typography>
              </TableCell>
        </TableBody>
        </Table>
    </TableContainer>
    <TableContainer component={Paper} className={classes.tableContainer}>
      <Table className={classes.table}>
        <TableHead>
          <TableRow>
            <TableCell className={classes.tableHeaderCell}>Description</TableCell>
          </TableRow>
          </TableHead>
          <TableBody>
            <TableCell>
              <Typography className="borders">{post.description}</Typography>
            </TableCell>
          </TableBody>
      </Table>
    </TableContainer>
    <TableContainer component={Paper} className={classes.tableContainer}>
      <Table className={classes.table}>
        <TableHead>
          <TableRow>
            <TableCell className={classes.tableHeaderCell}>Input</TableCell>
            <TableCell className={classes.tableHeaderCell}>Output</TableCell>
          </TableRow>
          </TableHead>
          <TableBody>
          
            {post.examples.map((item)=>(
              <TableRow>
            <TableCell>
            {item.input}
          </TableCell>
          <TableCell>
          {item.output}
          </TableCell>
          </TableRow>
            ))}

          </TableBody>
      </Table>
    </TableContainer>
    </>)}
    
    <TableContainer component={Paper} className={classes.tableContainer}>
      <Table className={classes.table}>
        <TableHead>
          <TableRow>
            <TableCell className={classes.tableHeaderCell}>Send file</TableCell>
          </TableRow>
          </TableHead>
          <TableBody>
            <TableCell>
            {darg ? <div className="drop-area"
          onDragStart={e =>dargStartHandler(e)}
          onDragLeave={e =>dargLeaveHandler(e)}
          onDragOver={e =>dargStartHandler(e)}
          onDrop={e=>onDropHandler(e)}
          >Отпустите файл для загрузки</div>
          :<div
          onDragStart={e =>dargStartHandler(e)}
          onDragLeave={e =>dargLeaveHandler(e)}
          onDragOver={e =>dargStartHandler(e)}
          >Перетащите файл, чтобы загрузить его</div>
          }
            </TableCell>
          </TableBody>
      </Table>
    </TableContainer>
    <TableContainer component={Paper} className={classes.tableContainer}>
      <Table className={classes.table}>
        <TableHead>
          <TableRow>
          <TableCell className={classes.tableHeaderCell}>File</TableCell>

            <TableCell className={classes.tableHeaderCell}>Created On</TableCell>
            <TableCell className={classes.tableHeaderCell}>Comment</TableCell>
            <TableCell className={classes.tableHeaderCell}>Success</TableCell>
          </TableRow>
          </TableHead>
          <TableBody>
          
            {mySolutions?.map((item)=>(
              <TableRow>
              <TableCell>
            <a href={item?.file}>File</a>
          </TableCell>
            <TableCell>
            {item?.created_on}
          </TableCell>
          <TableCell>
            {item?.comment}
          </TableCell>
          <TableCell>
          {item?.success ? "Yes" : "No"}
          </TableCell>
          </TableRow>
            ))}

          </TableBody>
      </Table>
    </TableContainer>
  </div>
)}

export  {TaskDescription}