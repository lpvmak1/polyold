import React from 'react';
import {
    Typography,
    Button,
    Table,
    TableBody, 
    TableCell, 
    TableContainer, 
    TableHead, 
    TableRow, 
    makeStyles,
    Paper
  } from "@material-ui/core";
import { Link } from "react-router-dom";

const useStyles = makeStyles((theme) => ({
  table: {
    minWidth: 1,
  },
  tableContainer: {
    borderRadius: 15,
    margin: '10px auto',
    maxWidth: 700,
  },
  tableHeaderCell: {
    fontWeight: 'bold',
    backgroundColor: theme.palette.primary.dark,
    color: theme.palette.getContrastText(theme.palette.primary.dark),
  }, 
  status: {
    fontWeight: 'bold',
    fontSize: '0.75rem',
    color: 'white',
    backgroundColor: 'grey',
    borderRadius: 8,
    padding: '3px 10px',
    display: 'inline-block'
  },
}));

export default props => (
    <TableContainer component={Paper} className={useStyles().tableContainer}>
      <Table className={useStyles().table}>
        <TableHead>
          <TableRow>
            <TableCell className={useStyles().tableHeaderCell}>Task</TableCell>
            <TableCell className={useStyles().tableHeaderCell}>Score</TableCell>
            <TableCell className={useStyles().tableHeaderCell}>Level</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {props.data.map((item) => (
            <TableRow key={item.id}>
              <TableCell>
                <Typography variant="body1">
                    <Button color="primary" component={Link} to={`/task/${item.id}`}>
                      {item.name}
                    </Button>
                </Typography>
              </TableCell>
              <TableCell>{item.score_points}</TableCell>
              <TableCell>
                <Typography
                className={useStyles().status} 
                style={{
                        backgroundColor: 
                        ((item.tags == 'Easy' && 'green') ||
                        (item.tags == 'Normal' && 'orange') ||
                        (item.tags == 'Hard' && 'red'))
                    }}>
                    {item.tags}
                </Typography>
              </TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </TableContainer>
)
