import React, {Component} from "react";
import Table from './Table';

export default class Header extends Component {
    constructor(props){
        super(props);
        this.state = {
            error: null,
            isLoaded: false,
            items: []
        };
    }

    componentDidMount() {
        fetch('https://polyold.lapotnikov.ru/api/challenges/', {headers: {Accept:'application/json'}})
        .then(res => res.json())
        .then (
            (result) => {
                this.setState({
                    isLoaded: true,
                    items: result
                });
            },
            (error) => {
                this.setState({
                    isLoaded: false,
                    error
                });
            }
         )
}
    
    render(){
        const {error, isLoaded, items} = this.state;
        if(error){
            return <p> Error {error.message} </p>
        } else if(!isLoaded){
            return <p> Loading ... </p>
        } else{
            return (
                <Table 
                    data = {this.state.items}
                />
            )
        }
    }
}


