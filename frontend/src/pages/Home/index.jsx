import { Grid, makeStyles, Container, Typography } from "@material-ui/core";
import "./direct.css"

const useStyles = makeStyles((theme) => ({
  center: {
    margin: '5px auto',
    textAlign: 'center'
  }
}));



function Home() {
  const classes = useStyles();

  return (
    <div>
    <Typography className={classes.center}><h1>Polyold</h1></Typography>
    <Typography className={classes.center}>Всем привет, мы команда Grandfathers, разработали этот сайт, чтобы помочь начинающим программистам.</Typography>
    <Typography className={classes.center}>В целях лучшего обучения, призываем вас соблюдать ряд правил кодекса чести.</Typography>
    <Typography className={classes.center} style ={{margin: '30px auto'}}><h2>Кодекс чести</h2></Typography>
    <Typography className={classes.center}>
      <li>Запрещаеться использовать код написаннай не Вами.</li>
      <li>Просить товарища, друга, коллегу написать код за вас.</li>
      <li>Загружать вредоносные программы, которые могут испортить работу сайта.</li>
    </Typography>
    <Typography className={classes.center} style ={{margin: '20px auto'}}>Надеемся на ваше благоразумие. Ведь это нужно вам, а не кому-то еще. </Typography>

    <Typography className={classes.center} style ={{margin: '0px 10px'}}>Чтобы приступить к выполнению задания, вы должны быть зарегистрированны. Если вы этого еще не сделали, то бегом регистроваться!</Typography>
    <Typography className={classes.center} style ={{margin: '0px 10px'}}>Задания делятся на три уровня сложности: </Typography>
    <Typography className={classes.center} style ={{color: 'green', margin: '0px 10px'}}><li>Easy - достаточно простые</li></Typography>
    <Typography className={classes.center} style ={{color: 'orange', margin: '10px 10px'}}><li>Normal - придется потрать немного времени</li></Typography>
    <Typography className={classes.center} style ={{color: 'red', margin: '0px 10px'}}><li>Hard - возможно, потратите несколько часов</li></Typography>
    <Typography className={classes.center} style ={{margin: '10px 10px'}}>За каждое задание вы можете получить определенное количество баллов, которые будут учитываться в рейтинговой системе.</Typography>
    <Typography className={classes.center} style ={{margin: '10px 10px'}}>Удачи в обучении!</Typography>
    </div>
  );
}

export default Home;
