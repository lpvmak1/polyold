import * as yup from "yup";

const schema = yup.object().shape({
  first_name: yup.string().required(),
  last_name: yup.string().required(),
});

export default schema;
